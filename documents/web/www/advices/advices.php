<?php
$table=htmlspecialchars($_GET["table"]);
$conn = new PDO('pgsql:host=localhost;port=5432;dbname=advices', 'advices', 'poor-password');
$sql = 'select * from ' . $table . ';';
//echo $sql;
$resultset = $conn->prepare($sql);
$resultset->execute();
echo '<table border=1>';
$first=1;
while ($row = $resultset->fetch(PDO::FETCH_ASSOC)) {
    $columns = array_keys($row);
    if($first==1){
        echo '<tr>';
        for ($z=0; isset($columns[$z]);$z++){    
            echo '<th>', $columns[$z], '</th>';
        }
        echo '</tr>';
        $first=0;
    }
    echo '<tr>';
    for ($z=0; isset($columns[$z]);$z++){  
        echo '<td>',$row[$columns[$z]],'</td>';
    }
    echo '</tr>';
}
echo '</table>';
?>
