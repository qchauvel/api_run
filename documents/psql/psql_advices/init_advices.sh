#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
	CREATE USER advices WITH PASSWORD 'poor-password';
	CREATE DATABASE advices OWNER list_cmd;
	SELECT datname FROM pg_database; --debug
	GRANT ALL PRIVILEGES ON DATABASE advices TO advices;
EOSQL

psql -v ON_ERROR_STOP=1 --username advices --dbname advices <<-EOSQL
    \i ./table.sql
EOSQL