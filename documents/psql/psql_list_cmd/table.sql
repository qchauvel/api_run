DROP TABLE IF-EXISTS cmd;
CREATE TABLE cmd(
    name VARCHAR(255) PRIMARY KEY,
    descr TEXT
);

INSERT INTO cmd VALUES("man","The man command provides a user manual of any commands or utilities you can run in Terminal, including the name, description, and options.");
INSERT INTO cmd VALUES("sudo","Short for superuser do, sudo is one of the most common Linux commands that lets you perform tasks that require administrative or root permissions.");
INSERT INTO cmd VALUES("cd","To navigate through the Linux files and directories, use the cd command. Depending on your current working directory, it requires either the full path or the directory name.");
INSERT INTO cmd VALUES("ls","The ls command lists files and directories within a system. Running it without a flag or parameter will show the current working directory’s content.");
INSERT INTO cmd VALUES("cat","Concatenate, or cat, is one of the most frequently used Linux commands. It lists, combines, and writes file content to the standard output. To run the cat command, type cat followed by the file name and its extension.");
INSERT INTO cmd VALUES("cp","Use the cp command to copy files or directories and their content. ");
INSERT INTO cmd VALUES("mv","The primary use of the mv command is to move and rename files and directories. Additionally, it doesn’t produce an output upon execution.");
INSERT INTO cmd VALUES("mkdir","Use the mkdir command to create one or multiple directories at once and set permissions for each of them. The user executing this command must have the privilege to make a new folder in the parent directory, or they may receive a permission denied error.");
INSERT INTO cmd VALUES("rm","The rm command is used to delete files within a directory.");
INSERT INTO cmd VALUES("find","Use the find command to search for files within a specific directory and perform subsequent operations.");
INSERT INTO cmd VALUES("grep","Another basic Linux command on the list is grep or global regular expression print. It lets you find a word by searching through all the texts in a specific file.");
