#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
	CREATE USER list_cmd WITH PASSWORD 'poor-password';
	CREATE DATABASE list_cmd OWNER list_cmd;
	SELECT datname FROM pg_database; --debug
	GRANT ALL PRIVILEGES ON DATABASE list_cmd TO list_cmd;
EOSQL
psql -v ON_ERROR_STOP=1 --username list_cmd --dbname list_cmd <<-EOSQL
    \i ./table.sql
EOSQL