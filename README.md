# api_run

## Demo
- "Site projet" : http://info.bsd.picagraine.net/info.php
- "Site commandes" : http://list_cmd.bsd.picagraine.net/list_cmd.php (ne marche pas pour l'instant)
- "Site conseils" : http://advices.bsd.picagraine.net/advices.php (ne marche pas pour l'instant)

## pour lancer
- `git clone https://gitlab.utc.fr/qchauvel/api_run.git && cd ./api_run/documents`
- `mkdir bdd`
- `docker network create myfirstnet`
- ` export pwd=$(pwd) && docker compose up` (ne marche pas pour l'instant, je n'arrive pas à faire fonctionner le container Postgres)

## TODO
- [ ] avoir les bases de données qui sont fonctionnelles
- [ ] remplir la base de données advices
- [ ] faire l'architecture demandé
- [ ] faire en sorte que les site-available/[files] de nginx utilise un server_name correct (pour l'instant ca utilise le nom de domaine de mon vps)
- [ ] _peut-etre_ utiliser info.sql comme l'index d'info, list_cmd.php comme l'index de list_cmd and advices.php comme l'index de advices
- [ ] _peut-etre_ envoyer tous les fichier à php_fpm
- [ ] ...

